# -*- coding: utf-8 -*-

import os
import os.path
import xml.dom.minidom

FIND_ROOT  =  "."

def main():
      jarinfos_all = map(get_jarinfos_from_pom, find_poms(FIND_ROOT))
      jarinfos = reduce(merge_jarinfos_without_test, jarinfos_all, [])
      print "<dependencies>"
      for jarinfo in jarinfos:
            print jarinfo
      print "</dependencies>"

def find_poms(root_path):
      poms = []
      for pom in find_pom(root_path):
            poms.append(pom)
      return poms

def find_pom(root_path):
      for root, dirs, files in os.walk(root_path):
            if "pom.xml" in files:
                  yield os.path.join(root, "pom.xml")
            for exclude_dir in filter(exclude_dirs, dirs):
                  dirs.remove(exclude_dir)

def exclude_dirs(src_dirs):
      dist_dirs = []
      if "src" in src_dirs:
            dist_dirs.append("src")
      if "target" in src_dirs:
            dist_dirs.append("target")
      return dist_dirs

def get_jarinfos_from_pom(pom_path):
      dom = xml.dom.minidom.parse(pom_path)
      elements = dom.getElementsByTagName("dependencies")
      for dependencies in elements:
            parent = dependencies.parentNode
            if parent.localName != "dependencyManagement":
                  return get_jarinfos_from_dependencies_tag(dependencies)
      return []

def get_jarinfos_from_dependencies_tag(dependencies):
      elements = dependencies.getElementsByTagName("dependency")
      return map(get_jarinfos_from_dependency_tag, elements)

def get_jarinfos_from_dependency_tag(dependency):
      group_id = get_group_id_from_dependency_tag(dependency)
      artifact_id = get_artifact_id_from_dependency_tag(dependency)
      scope = get_scope_from_dependency_tag(dependency)
      return JarInfo(group_id, artifact_id, scope)

def get_group_id_from_dependency_tag(dependency):
      elements = dependency.getElementsByTagName("groupId")
      if 1 == len(elements):
            return get_node_text(elements[0].childNodes)
      return ""

def get_artifact_id_from_dependency_tag(dependency):
      elements = dependency.getElementsByTagName("artifactId")
      if 1 == len(elements):
            return get_node_text(elements[0].childNodes)
      return ""

def get_scope_from_dependency_tag(dependency):
      elements = dependency.getElementsByTagName("scope")
      if 1 == len(elements):
            return get_node_text(elements[0].childNodes)
      return "compile"

def get_node_text(nodes):
      for node in nodes:
            if node.nodeType == node.TEXT_NODE:
                  return node.data
      return ""

def merge_jarinfos_without_test(jarinfos, jarinfos_by_pom):
      for item in jarinfos_by_pom:
            if item.is_scope_test():
                  continue
            if item not in jarinfos:
                  jarinfos.append(item)
      return jarinfos

class JarInfo(object):
      _group_id = ""
      _artifact_id = ""
      _scope = ""
      
      def __init__(self, group_id, artifact_id, scope):
            self._group_id = group_id
            self._artifact_id = artifact_id
            self._scope = scope

      def __cmp__(self, other):
            rst = cmp(self._group_id, other._group_id)
            if 0 != rst:
                  return rst
            rst = cmp(self._artifact_id, other._artifact_id)
            if 0 != rst:
                  return rst
            return 0

      def __str__(self):
            return "<groupId>{groupId}</groupId><artifactId>{artifactId}</artifactId>".format(groupId = self._group_id, artifactId = self._artifact_id)

      def is_scope_test(self):
            if self._scope == "test":
                  return True
            return False

if __name__ == '__main__':
      main()
